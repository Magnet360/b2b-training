global virtual class cc_shop360_hk_UserInterface_v4 extends ccrz.cc_hk_UserInterface.v004 {
    global cc_shop360_hk_UserInterface_v4(){}

    global boolean useBoot3 {
        get {
            if (null == useBoot3) {
                useBoot3 = ccrz.cc_CallContext.isConfigEqual('ui.tmplver', 'boot3', null);
            }
            return useBoot3;
        }
        private set;
    }

    global virtual override String ccrzIncludes(){
        if(useBoot3){
            if(minified){
                return '<script type="text/javascript" src="'+resourcePath('ccrz__CC_JAVASCRIPT_FRAMEWORK', 'js/cc3-min.js')+'"></script>\n';
            }else{
                return
                    '<script type="text/javascript" src="'+resourcePath('ccrz__CC_JAVASCRIPT_FRAMEWORK', 'js/jquery.currency.js')+'"></script>\n'+
                    '<script type="text/javascript" src="'+resourcePath('ccrz__CC_JAVASCRIPT_FRAMEWORK', 'js/cloudcraze.js')+'"></script>\n'+
                    '<script type="text/javascript" src="'+resourcePath('ccrz__CC_JAVASCRIPT_FRAMEWORK', 'js/CC_Backbone.js')+'"></script>\n'+
                    '<script type="text/javascript" src="'+resourcePath('ccrz__CC_JAVASCRIPT_FRAMEWORK', 'js/backbone_models.js')+'"></script>\n'+
                    '';
            }
        }else{
            if(minified){
                return
                    '<script type="text/javascript" src="'+resourcePath('ccrz__CC_JAVASCRIPT_FRAMEWORK', 'js/cc-min.js')+'"></script>\n';
            }else{
                return
                    '<script type="text/javascript" src="'+resourcePath('ccrz__CC_JAVASCRIPT_FRAMEWORK', 'js/jquery.currency.js')+'"></script>\n'+
                    '<script type="text/javascript" src="'+resourcePath('ccrz__CC_JAVASCRIPT_FRAMEWORK', 'js/controllers_new_slider.js')+'"></script>\n'+
                    '<script type="text/javascript" src="'+resourcePath('ccrz__CC_JAVASCRIPT_FRAMEWORK', 'js/cloudcraze.js')+'"></script>\n'+
                    '<script type="text/javascript" src="'+resourcePath('ccrz__CC_JAVASCRIPT_FRAMEWORK', 'js/CC_Backbone.js')+'"></script>\n'+
                    '<script type="text/javascript" src="'+resourcePath('ccrz__CC_JAVASCRIPT_FRAMEWORK', 'js/backbone_models.js')+'"></script>\n';
            }
        }
    }

    global virtual override String standardRespondJS(){
        if(useBoot3){
            return '';
        }else{
            return '<script type="text/javascript" src="'+resourcePath('ccrz__CCRZ_JS','v004/classic/respond-1.4.2.js')+'"></script>\n';
        }
    }

    global virtual override String standardIncludes(){
        String retElements = '';
        if(useBoot3){
            if(minified){
                retElements +=
                        '<link type="text/css" rel="stylesheet" href="'+resourcePath('ccrz__CCRZ_JS', 'v004/boot3/css/ccrz-libs.min.css')+'" />\n'+
                        '';
            }else{
                retElements +=
                        '<link type="text/css" rel="stylesheet" href="'+resourcePath('ccrz__CCRZ_JS', 'v004/boot3/css/bootstrap-datepicker3.css')+'" />\n'+
                        '<link type="text/css" rel="stylesheet" href="'+resourcePath('ccrz__CCRZ_JS', 'v004/boot3/css/font-awesome.min.css')+'" />\n'+
                        '<link type="text/css" rel="stylesheet" href="'+resourcePath('ccrz__CCRZ_JS', 'v004/boot3/css/jquery-ui-custom.css')+'" />\n'+
                        '<link type="text/css" rel="stylesheet" href="'+resourcePath('ccrz__CCRZ_JS', 'v004/boot3/css/bootstrap.css')+'" />\n'+
                        '<link type="text/css" rel="stylesheet" href="'+resourcePath('ccrz__CCRZ_JS', 'v004/boot3/css/shimbs3.css')+'" />\n'+
                        '<link type="text/css" rel="stylesheet" href="'+resourcePath('ccrz__CCRZ_JS', 'v004/boot3/css/jquery.nouislider.css')+'" />\n'+
                        '';
            }

            if(minified){
                retElements +=
                        '<script type="text/javascript" src="'+resourcePath('ccrz__CCRZ_JS', 'v004/boot3/ccrz-libsh-min.js')+'"></script>\n';
            }else{
                retElements +=
                        '<script type="text/javascript" src="'+resourcePath('ccrz__CCRZ_JS', 'v004/boot3/jquery-3.3.1.js')+'"></script>\n'+
                        '<script type="text/javascript" src="'+resourcePath('ccrz__CCRZ_JS', 'v004/boot3/jquery-migrate-3.0.1.js')+'"></script>\n'+
                        '<script type="text/javascript" src="'+resourcePath('ccrz__CCRZ_JS', 'v004/boot3/jquery-ui-custom.1.12.1.js')+'"></script>\n'+
                        '<script type="text/javascript" src="'+resourcePath('ccrz__CCRZ_JS', 'v004/boot3/jquery.validate.1.17.0.js')+'"></script>\n'+
                        '<script type="text/javascript" src="'+resourcePath('ccrz__CCRZ_JS', 'v004/boot3/underscore-1.9.1.js')+'"></script>\n'+
                        '<script type="text/javascript" src="'+resourcePath('ccrz__CCRZ_JS', 'v004/boot3/backbone-1.3.3.js')+'"></script>\n'+
                        '<script type="text/javascript" src="'+resourcePath('ccrz__CCRZ_JS', 'v004/boot3/accounting-0.4.2.js')+'"></script>\n'+
                        '<script type="text/javascript" src="'+resourcePath('ccrz__CCRZ_JS', 'v004/boot3/handlebars-v4.0.11.js')+'"></script>\n'+
                        '';
            }
            retElements += ccrzIncludes();

            return retElements;
        }else{
            if(minified){
                retElements +=
                        '<script type="text/javascript" src="'+resourcePath('ccrz__CCRZ_JS', 'v004/classic/ccrz-libs-min.js')+'"></script>\n';
            }else{
                retElements +=
                        '<script type="text/javascript" src="'+resourcePath('ccrz__CCRZ_JS', 'v004/classic/jquery-3.3.1.js')+'"></script>\n'+
                        '<script type="text/javascript" src="'+resourcePath('ccrz__CCRZ_JS', 'v004/classic/jquery-migrate-3.0.1.js')+'"></script>\n'+
                        '<script type="text/javascript" src="'+resourcePath('ccrz__CCRZ_JS', 'v004/classic/jquery-ui-custom.1.12.1.js')+'"></script>\n'+
                        '<script type="text/javascript" src="'+resourcePath('ccrz__CCRZ_JS', 'v004/classic/jquery.cookie-1.4.1.js')+'"></script>\n'+
                        '<script type="text/javascript" src="'+resourcePath('ccrz__CCRZ_JS', 'v004/classic/jquery.hoverIntent-1.9.0.js')+'"></script>\n'+
                        '<script type="text/javascript" src="'+resourcePath('ccrz__CCRZ_JS', 'v004/classic/jquery-vertical-accordion-menu/js/jquery.dcjqaccordion.2.7.js')+'"></script>\n'+
                        '<script type="text/javascript" src="'+resourcePath('ccrz__CCRZ_JS', 'v004/classic/jquery.validate.1.17.0.js')+'"></script>\n'+
                        '<script type="text/javascript" src="'+resourcePath('ccrz__CCRZ_JS', 'v004/classic/underscore-1.9.1.js')+'"></script>\n'+
                        '<script type="text/javascript" src="'+resourcePath('ccrz__CCRZ_JS', 'v004/classic/backbone-1.3.3.js')+'"></script>\n'+
                        '<script type="text/javascript" src="'+resourcePath('ccrz__CCRZ_JS', 'v004/classic/modernizr-2.8.3.js')+'"></script>\n'+
                        '<script type="text/javascript" src="'+resourcePath('ccrz__CCRZ_JS', 'v004/classic/accounting-0.4.2.js')+'"></script>\n'+
                        '<script type="text/javascript" src="'+resourcePath('ccrz__CCRZ_JS', 'v004/classic/handlebars-v4.0.11.js')+'"></script>\n'+
                        '<script type="text/javascript" src="'+resourcePath('ccrz__CCRZ_JS', 'v004/classic/js-image-slider-2016.9.27.js')+'"></script>\n'+
                        '<script type="text/javascript" src="'+resourcePath('ccrz__CCRZ_JS', 'v004/classic/bootstrap-datepicker-1.8.0.js')+'"></script>\n'+
                        '<script type="text/javascript" src="'+resourcePath('ccrz__CCRZ_JS', 'v004/classic/js2form.2011.9.19.js')+'"></script>\n'+
                        '<script type="text/javascript" src="'+resourcePath('ccrz__CCRZ_JS', 'v004/classic/form2js-2010.9.9.js')+'"></script>\n'+
                        '<script type="text/javascript" src="'+resourcePath('ccrz__CCRZ_JS', 'v004/classic/backbone.paginator.2.0.6.js')+'"></script>\n'+
                        '<script type="text/javascript" src="'+resourcePath('ccrz__CCRZ_JS', 'v004/classic/bootstrap-2.3.2/js/bootstrap.js')+'"></script>\n'+
                        '<script type="text/javascript" src="'+resourcePath('ccrz__CCRZ_JS', 'v004/classic/jquery.nouislider.min.js')+'"></script>\n';
            }

            retElements +=
                    ccrzIncludes() +
                            '<link type="text/css" rel="stylesheet" href="'+resourcePath('ccrz__CCRZ_JS', 'v004/classic/css/bootstrap-datepicker.css')+'" />\n'+
                            '<link type="text/css" rel="stylesheet" href="'+resourcePath('ccrz__CCRZ_JS', 'v004/classic/css/jquery-ui-custom.css')+'" />\n'+
                            '<link type="text/css" rel="stylesheet" href="'+resourcePath('ccrz__CCRZ_JS', 'v004/classic/css/jquery.nouislider.css')+'" />\n';
            return retElements;
        }
    }

    global virtual override Map<String,Object> endContent(Map<String,Object> inpData){
        if(useBoot3){
            String retContent = '';
            if (minified) {
                retContent +=
                    '<script type="text/javascript" src="' + resourcePath('ccrz__CCRZ_JS', 'v004/boot3/ccrz-libse-min.js') + '"></script>\n';
            } else {
                retContent +=
                    '<script type="text/javascript" src="' + resourcePath('ccrz__CCRZ_JS', 'v004/boot3/bootstrap-datepicker-1.8.0.js') + '"></script>\n' +
                    '<script type="text/javascript" src="' + resourcePath('ccrz__CCRZ_JS', 'v004/boot3/js2form.2011.9.19.js') + '"></script>\n' +
                    '<script type="text/javascript" src="' + resourcePath('ccrz__CCRZ_JS', 'v004/boot3/form2js-2010.9.9.js') + '"></script>\n' +
                    '<script type="text/javascript" src="' + resourcePath('ccrz__CCRZ_JS', 'v004/boot3/backbone.paginator.2.0.6.js') + '"></script>\n' +
                    '<script type="text/javascript" src="' + resourcePath('ccrz__CCRZ_JS', 'v004/boot3/jquery.nouislider.min.js') + '"></script>\n' +
                    '<script type="text/javascript" src="' + resourcePath('ccrz__CCRZ_JS', 'v004/boot3/bootstrap.3.3.7-cc.js') + '"></script>\n' +
                    '';
            }

            return new Map<String, Object>{
                EOB_CONTENT => retContent
            };
        }else {
            return new Map<String,Object>{
                EOB_CONTENT => ''
            };
        }
    }

    global virtual override String metaContent(){
        if(useBoot3){
            return BS3_STANDARD_META_VIEWPORT;
        }else{
            return STANDARD_META_VIEWPORT;
        }
    }

    global virtual override Map<String,Object> headContent(Map<String,Object> inpData){
        if(useBoot3){
            String retContent = metaContent();

            //add JS for overlay override from Theme file
            retContent += '<script type="text/javascript" src="'+themeResourcePath('js/overlayOverride.js')+'"></script>\n';

            if(!ccrz.cc_CallContext.isConfigTrue('ui.ovrincl', null)){
                retContent += standardIncludes();
            }

            retContent +=(
                    pageConfigCSS3Includes()   +
                            standardRespondJS()    +
                            pageConfigJS3Includes()    +
                            standardUIProperties()
            );

            return new Map<String,Object>{
                    HEAD_CONTENT => retContent
            };
        }else{
            String retContent = metaContent();

            if(!ccrz.cc_CallContext.isConfigTrue('ui.ovrincl', null)){
                retContent += standardIncludes();
            }

            retContent +=(
                pageConfigCSSIncludes()   +
                standardRespondJS()    +
                pageConfigJSIncludes()    +
                standardUIProperties()
            );

            return new Map<String,Object>{
                HEAD_CONTENT => retContent
            };
        }
    }

    global virtual override String standardUIProperties(){
        if(minified){
            return
                '<script type="text/javascript" src="'+themeResourcePath('js/uiproperties.js')+'"></script>\n';

        }else{
            return
                '<script type="text/javascript" src="'+resourcePath('ccrz__CC_JAVASCRIPT_FRAMEWORK','js/uiproperties.js')+'"></script>\n' +
                '<script type="text/javascript" src="'+themeResourcePath('js/uiproperties.js')+'"></script>\n';
        }
    }
}