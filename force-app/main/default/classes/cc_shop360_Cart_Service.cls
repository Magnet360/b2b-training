/*****************************************************
 * Author: Zach Engman
 * Created Date: 07/16/2019
 * Created By: Zach Engman
 * Last Modified Date: 07/16/2019
 * Last Modified By:
 * Description: Service class for CloudCraze Cart API calls
 * ****************************************************/
public with sharing class cc_shop360_Cart_Service {
    //default sizing to return cart items, product and product details
    public Map<String, Object> ccrzAPISizing = new Map<String, Object>{ccrz.ccApiCart.ENTITYNAME => new Map<String, Object>{ccrz.ccAPI.SZ_DATA => ccrz.ccAPI.SZ_XL}
																	  ,ccrz.ccApiProduct.ENTITYNAME => new Map<String, Object>{ccrz.ccAPI.SZ_DATA => ccrz.ccAPI.SZ_L}};

    public cc_shop360_Cart_Service(string storefront, string userId){
        this(storefront, userId, null);
    }

    public cc_shop360_Cart_Service(string storefront, string userId, string encryptedCartId) {
        ccrz.cc_RemoteActionContext ccrzContext = new ccrz.cc_RemoteActionContext();
        ccrzContext.portalUserId = userId;
		ccrzContext.storefront = storefront; //'Shop360';

        if(!String.isBlank(encryptedCartId)){
            ccrzContext.currentCartId = encryptedCartId;
        }

        ccrz.cc_CallContext.initRemoteContext(ccrzContext);
    }

    /**
	* Fetches cart data from cloudcraze using the encrypted cart id.
	* @param encryptedCartId the encrypted id of the cart to return the data for
	* @return the cart record containing cart items
	*/
	public CartRecord getCartByEncryptedId(string encryptedCartId){
		Map<String, Object> inputDataMap = new Map<String, Object>{
			ccrz.ccApi.API_Version => 6
		   ,ccrz.ccApiCart.ACTIVECART => true
		   ,ccrz.ccApiCart.CART_ENCID => encryptedCartId
		   ,ccrz.ccApi.SIZING => this.ccrzAPISizing
		};

		return getCart(inputDataMap);
	}

    /**
	* Fetches cart data from cloudcraze
	* @param inputDataMap the cloud craze formatted request parameters
	* @return the cart record and optionally containing cart items, address, products and product specs
	*/
	private CartRecord getCart(Map<String, Object> inputDataMap){
		CartRecord cartRecord;

		try{
			Map<String, Object> outputDataMap = ccrz.ccApiCart.fetch(inputDataMap);
			List<Map<String, Object>> outputProductList;

            if(outputDataMap.get(ccrz.ccApiCart.CART_OBJLIST) != null){
				List<Map<String, Object>> outputCartList = (List<Map<String, Object>>)outputDataMap.get(ccrz.ccApiCart.CART_OBJLIST);

				if(outputDataMap.get(ccrz.ccApiProduct.PRODUCTLIST) != null){
					outputProductList = (List<Map<String, Object>>)outputDataMap.get(ccrz.ccApiProduct.PRODUCTLIST);
				}

				if(outputCartList != null && outputCartList.size() > 0){
					cartRecord = new CartRecord(outputCartList[0], outputProductList);
				}
			}
		}
		catch(Exception ex){
			System.Debug(ex);
		}

		return cartRecord;
	}

    //Wrapper class to hold cart record and related object data
	public class CartRecord{
		public string id {get; private set;}
		public decimal adjustmentAmount {get; private set;}
		public string billTo {get; private set;}
		public string currencyISOCode {get; private set;}
		public string encryptedId {get; private set;}
		public string ownerId {get; private set;}
		public decimal shipAmount {get; private set;}
		public string shipTo {get; private set;}
		public decimal subtotalAmount {get; private set;}
		public decimal totalAmount {get; private set;}
		public decimal totalDiscount {get; private set;}
		public integer totalQuantity {get; private set;}
		public integer cartItemCount {get; private set;}
		public List<CartItem> cartItems {get; private set;}
		public List<Map<String, Object>> productList {get; private set;}

		public CartRecord(Map<String, Object> cartDataMap, List<Map<String, Object>> productList){
			this.id = (String)cartDataMap.get('sfid');
			this.adjustmentAmount = (Decimal)cartDataMap.get('adjustmentAmount');
			this.billTo = (String)cartDataMap.get('billTo');
			this.currencyISOCode = (String)cartDataMap.get('currencyISOCode');
			this.encryptedId = (String)cartDataMap.get('encryptedId');
			this.ownerId = (String)cartDataMap.get('ownerId');
			this.shipAmount = (Decimal)cartDataMap.get('shipAmount');
			this.shipTo = (String)cartDataMap.get('shipTo');
			this.subtotalAmount = (Decimal)cartDataMap.get('subtotalAmount');
			this.totalAmount = (Decimal)cartDataMap.get('totalAmount');
			this.totalDiscount = (Decimal)cartDataMap.get('totalDiscount');
			this.totalQuantity = Integer.valueOf(cartDataMap.get('totalQuantity'));

            //populate the cart item list
			this.cartItems = new List<CartItem>();

			List<Map<String, Object>> cartItemMapList = (List<Map<String, Object>>)cartDataMap.get('ECartItemsS');

			for(Map<String, Object> cartItemDataMap : cartItemMapList){
				cartItems.add(new CartItem(cartItemDataMap));
			}
            //store the major quantity item count
			this.cartItemCount = 0;
			for(CartItem ci : cartItems){
				if(ci.cartItemType == 'Major'){
					this.cartItemCount += ci.quantity;
				}
			}

			this.productList = productList;

            if(productList != null){
                for(CartItem item : cartItems){
                    if(item.cartItemType == 'Major'){
                        for(Map<String, Object> productRecord : productList){
                            if((String)productRecord.get('sfid') == item.productId){
                                item.productName = (String)productRecord.get('sfdcName');
                                break;
                            }
                        }
                    }
                }
            }
		}
	}
     //Wrapper class to hold cart item records 
	public class CartItem implements Comparable{
		public string id {get; private set;}
		public decimal absoluteDiscount {get; private set;}
		public string cartItemType {get; private set;}
		public decimal itemTotal {get; private set;}
		public decimal price {get; private set;}
		public integer quantity {get; private set;}
		public string productId {get; private set;}
		public string productName {get; private set;}
		public string productType {get; private set;}
		public string sku {get; private set;}
		
		public CartItem(Map<String, Object> cartItemDataMap){
			this.id = (String)cartItemDataMap.get('sfid');
			this.absoluteDiscount = (Decimal)cartItemDataMap.get('absoluteDiscount');
			this.cartItemType = (String)cartItemDataMap.get('cartItemType');
			this.itemTotal = (Decimal)cartItemDataMap.get('itemTotal');
			this.price = (Decimal)cartItemDataMap.get('price');
			this.quantity = Integer.valueOf(cartItemDataMap.get('quantity'));

			Map<String, Object> productDataMap = (Map<String, Object>)cartItemDataMap.get('productR');

            if(productDataMap != null){
                this.productId = (String)productDataMap.get('sfid');
			    this.productType = (String)productDataMap.get('productType');
			    this.sku = (String)productDataMap.get('SKU');
            }
		}

		public Integer compareTo(Object compareTo){
            CartItem compareToCartItem = (CartItem)compareTo;
            return productName == compareToCartItem.productName ? 0 :
            productName > compareToCartItem.productName ? 1 : -1;
        }
	}
}