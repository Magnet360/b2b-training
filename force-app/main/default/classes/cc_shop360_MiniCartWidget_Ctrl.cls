/*****************************************************
 * Author: Zach Engman
 * Created Date: 07/16/2019
 * Created By: Zach Engman
 * Last Modified Date: 07/16/2019
 * Last Modified By:
 * Description: Controller for the mini cart widget lwc component
 * ****************************************************/
public with sharing class cc_shop360_MiniCartWidget_Ctrl {
    /**
	* Fetches cart data from cloudcraze using the encrypted cart id.
    * @param userId the active user originating the call (or guest user id)
	* @param encryptedCartId the encrypted id of the cart to return the data for
	* @return the cart record containing cart items
	*/
    @AuraEnabled(cacheable=true)
    public static String getCart(string storefront, string encryptedCartId, integer refreshCount){
        Map<String, Object> returnMap = new Map<String, Object>{'success' => false};
        cc_shop360_Cart_Service.CartRecord cartRecord;

        if(!String.isBlank(encryptedCartId)){
            cc_shop360_Cart_Service cartService = new cc_shop360_Cart_Service(storefront, UserInfo.getUserId(), encryptedCartId);
            cartRecord = cartService.getCartByEncryptedId(encryptedCartId);

            returnMap.put('success', true);
            returnMap.put('encryptedCartId', encryptedCartId);
            returnMap.put('cart', cartRecord);
        }

        return JSON.serialize(returnMap);
    }
}