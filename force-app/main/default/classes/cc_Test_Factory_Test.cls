/*****************************************************
 * Author: Zach Engman
 * Created Date: 07/29/2019
 * Last Modified Date: 07/29/2019
 * Last Modified By:
 * Description: Test coverage of the cc_Test_Factory
 * ****************************************************/
@IsTest
private class cc_Test_Factory_Test {
    @IsTest
	private static void testCreateSingleObject(){
		Contact contactRecord;

		Test.startTest();

		contactRecord = (Contact)cc_Test_Factory.createSObject('Contact');

		Test.stopTest();

        //verify the object record was created
		System.assert(contactRecord != null);
	}

	@IsTest
	private static void testCreateSingleObjectInvalid(){
		Contact contactRecord;

		Test.startTest();

		try{
			contactRecord = (Contact)cc_Test_Factory.createSObject('ContactInvalid__c');
		}
		catch(cc_Test_Factory.cc_Test_Factory_Exception ex){}

		Test.stopTest();
        //verify the object record was not created
		System.assert(contactRecord == null);
	}

	@IsTest
	private static void testCreateMultipleObjects(){
		List<Account> accountList;

		Test.startTest();

		accountList = (List<Account>)cc_Test_Factory.createSObjectList('Account', 10);

		Test.stopTest();
        //verify the correct number of object records were created
		System.assert(accountList.size() == 10);
	}

	@IsTest
	private static void testCreateAccountObject(){
		Account accountRecord;

		Test.startTest();

		accountRecord = cc_Test_Factory.createAccount();

		Test.stopTest();
        //verify the account record was created
		System.assert(accountRecord != null);
	}

	@IsTest
	private static void testCreateCCAccountGroupObject(){
		ccrz__E_AccountGroup__c accountGroupRecord;

		Test.startTest();

		accountGroupRecord = cc_Test_Factory.createCCAccountGroup();

		Test.stopTest();
        //verify the account group record was created
		System.assert(accountGroupRecord != null);
	}

	@IsTest
	private static void testCreateCCAccountGroupPriceListObject(){
		ccrz__E_AccountGroupPriceList__c accountGroupPriceListRecord;

		ccrz__E_AccountGroup__c accountGroupRecord = cc_Test_Factory.createCCAccountGroup();
		insert accountGroupRecord;

		ccrz__E_PriceList__c priceListRecord = cc_Test_Factory.createCCPriceList();
		insert priceListRecord;

		Test.startTest();

		accountGroupPriceListRecord = cc_Test_Factory.createCCAccountGroupPriceList(accountGroupRecord.Id, priceListRecord.Id);

		Test.stopTest();
        //verify the account group price list record was created
		System.assert(accountGroupPriceListRecord != null);
	}

	@IsTest
	private static void testCreateCCCategoryObject(){
		ccrz__E_Category__c categoryRecord;

		Test.startTest();

		categoryRecord = cc_Test_Factory.createCCCategory();

		Test.stopTest();
        //verify the category record was created
		System.assert(categoryRecord != null);
	}

	@IsTest
	private static void testCreateCCCartObject(){
		ccrz__E_Cart__c cartRecord;

		Test.startTest();

		cartRecord = cc_Test_Factory.createCCCart();

		Test.stopTest();

		//verify the cart record was created
		System.assert(cartRecord != null);
	}

	@IsTest
	private static void testCreateCCCartItemObject(){
		ccrz__E_CartItem__c cartItemRecord;

		ccrz__E_Cart__c cartRecord = cc_Test_Factory.createCCCart();
		insert cartRecord;

		ccrz__E_Product__c productRecord = cc_Test_Factory.createCCProduct();
		insert productRecord;

		Test.startTest();

		cartItemRecord = cc_Test_Factory.createCCCartItem(cartRecord.Id, productRecord.Id);

		Test.stopTest();

		//verify the cart item record was created
		System.assert(cartItemRecord != null);
	}

    @IsTest
	private static void testCreateContactObject(){
		Contact contactRecord;

		Test.startTest();

		contactRecord = cc_Test_Factory.createContact();

		Test.stopTest();
        //verify the contact record was created
		System.assert(contactRecord != null);
	}

    @IsTest
    private static void testCreateUserObject(){
        User userRecord;

		Test.startTest();

		userRecord = cc_Test_Factory.createUser('System Administrator');

		Test.stopTest();
        //verify the user record was created
		System.assert(userRecord != null);
    }

	@IsTest
	private static void testSetupB2BCatalog(){
		Test.startTest();

		cc_Test_Factory.setupB2BCatalog();

		Test.stopTest();

		//verify records were created
		System.assert([SELECT Id FROM ccrz__E_Category__c].size() > 0);
	}
}