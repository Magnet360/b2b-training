/*****************************************************
 * Author: Zach Engman
 * Created Date: 08/05/2019
 * Created By: Zach Engman
 * Last Modified Date: 08/05/2019
 * Last Modified By:
 * Description: Provides test coverage of the functionality within the cc_shop360_hk_Menu class
 *    SEEALLDATA=true to utilize cc setup objects easily
 * ****************************************************/
@IsTest(SeeAllData=true)
private class cc_shop360_hk_Menu_Test {
    @IsTest
    private static void testFetchWithCategoryTreeEnabled(){
        cc_shop360_hk_Menu menuHook = new cc_shop360_hk_Menu();
        Map<String, Object> resultMap;

        menuHook.useCategoryTree = true;

        Test.startTest();

        resultMap = menuHook.fetch(new Map<String,Object>());

        Test.stopTest();
         //verify menu map was returned
        System.assert(resultMap != null);
    }

    @IsTest
    private static void testFetchWithCategoryTreeDisabled(){
        cc_shop360_hk_Menu menuHook = new cc_shop360_hk_Menu();
        Map<String, Object> resultMap;

        menuHook.useCategoryTree = false;

        Test.startTest();

        resultMap = menuHook.fetch(new Map<String,Object>());

        Test.stopTest();
        //verify menu map was returned
        System.assert(resultMap != null);
    }
}