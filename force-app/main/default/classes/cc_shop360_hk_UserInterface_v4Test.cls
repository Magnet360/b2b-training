@isTest
private class cc_shop360_hk_UserInterface_v4Test {
    @isTest
    static void testMethodBoot3Min() {
        String STOREFRONT = 'storefront1';

        Map<String,Map<String,Object>> testData = new Map<String,Map<String,Object>>{
            //Add hook override
            ccrz.ccApiTestData.HOOK_SETTINGS => new Map<String, Object>{
                STOREFRONT => new Map<String,Object>{
                    ccrz.cc_hk_UserInterface.HK_ID => 'c.cc_shop360_hk_UserInterface_v4'
                }
            },
            ccrz.ccApiTestData.CONFIG_SETTINGS => new Map<String,Object>{
                'ui.tmplver' => 'boot3',
                'ui.usemin' => 'TRUE',
                'th.name' => 'CC_Theme_Shop360'
            }
        };
        ccrz.ccApiTestData.setupData(testData);

        // Setup of the CCRZ context
        ccrz.cc_RemoteActionContext ctx = new ccrz.cc_RemoteActionContext();

        ctx.storefront = STOREFRONT;
        ctx.userLocale = 'en_US';

        ccrz.cc_CallContext.initRemoteContext(ctx);

        Map<String, Object> inputData = new Map<String, Object>{
            ccrz.ccAPI.API_VERSION => ccrz.ccAPI.CURRENT_VERSION,
            ccrz.ccAPI.SIZING => new Map<String, Object>{
                ccrz.ccApiCategory.ENTITYNAME => new Map<String, Object>{
                    ccrz.ccAPI.SZ_DATA => ccrz.ccAPI.SZ_L
                }
            }
        };

        Test.startTest();
        ccrz.cc_hk_UserInterface UserInterfaceHook = ccrz.cc_hk_UserInterface.getInstance(null);

        Map<String, Object> headContentMap = (Map<String, Object>)UserInterfaceHook.headContent(inputData);
        Map<String, Object> endContentMap = (Map<String, Object>)UserInterfaceHook.endContent(inputData);

        System.assertNotEquals(headContentMap, null);
        System.assertNotEquals(endContentMap, null);

        Test.stopTest();
    }

    @isTest
    static void testMethodBoot3NoMin() {
        String STOREFRONT = 'storefront1';

        Map<String,Map<String,Object>> testData = new Map<String,Map<String,Object>>{
            ccrz.ccApiTestData.HOOK_SETTINGS => new Map<String, Object>{
                STOREFRONT => new Map<String,Object>{
                    ccrz.cc_hk_UserInterface.HK_ID => 'c.cc_shop360_hk_UserInterface_v4'
                }
            },
            ccrz.ccApiTestData.CONFIG_SETTINGS => new Map<String,Object>{
                'ui.tmplver' => 'boot3',
                'ui.usemin' => 'FALSE',
                'th.name' => 'CC_Theme_Shop360'
            }
        };
        ccrz.ccApiTestData.setupData(testData);

        // Setup of the CCRZ context
        ccrz.cc_RemoteActionContext ctx = new ccrz.cc_RemoteActionContext();

        ctx.storefront = STOREFRONT;
        ctx.userLocale = 'en_US';

        ccrz.cc_CallContext.initRemoteContext(ctx);

        Map<String, Object> inputData = new Map<String, Object>{
            ccrz.ccAPI.API_VERSION => ccrz.ccAPI.CURRENT_VERSION,
            ccrz.ccAPI.SIZING => new Map<String, Object>{
                ccrz.ccApiCategory.ENTITYNAME => new Map<String, Object>{
                    ccrz.ccAPI.SZ_DATA => ccrz.ccAPI.SZ_L
                }
            }
        };

        Test.startTest();
        ccrz.cc_hk_UserInterface UserInterfaceHook = ccrz.cc_hk_UserInterface.getInstance(null);

        Map<String, Object> headContentMap = (Map<String, Object>)UserInterfaceHook.headContent(inputData);
        Map<String, Object> endContentMap = (Map<String, Object>)UserInterfaceHook.endContent(inputData);

        System.assertNotEquals(headContentMap, null);
        System.assertNotEquals(endContentMap, null);

        Test.stopTest();
    }

    @isTest
    static void testMethodClassic() {
        String STOREFRONT = 'storefront1';

        Map<String,Map<String,Object>> testData = new Map<String,Map<String,Object>>{
            ccrz.ccApiTestData.HOOK_SETTINGS => new Map<String, Object>{
                STOREFRONT => new Map<String,Object>{
                    ccrz.cc_hk_UserInterface.HK_ID => 'c.cc_shop360_hk_UserInterface_v4'
                }
            },
            ccrz.ccApiTestData.CONFIG_SETTINGS => new Map<String,Object>{
                'ui.tmplver' => 'classic',
                'th.name' => 'CC_Theme_Shop360'
            }
        };
        ccrz.ccApiTestData.setupData(testData);

        // Setup of the CCRZ context
        ccrz.cc_RemoteActionContext ctx = new ccrz.cc_RemoteActionContext();

        ctx.storefront = STOREFRONT;
        ctx.userLocale = 'en_US';
        
        ccrz.cc_CallContext.initRemoteContext(ctx);

        Map<String, Object> inputData = new Map<String, Object>{
            ccrz.ccAPI.API_VERSION => ccrz.ccAPI.CURRENT_VERSION,
            ccrz.ccAPI.SIZING => new Map<String, Object>{
                ccrz.ccApiCategory.ENTITYNAME => new Map<String, Object>{
                    ccrz.ccAPI.SZ_DATA => ccrz.ccAPI.SZ_L
                }
            }
        };

        Test.startTest();
        ccrz.cc_hk_UserInterface UserInterfaceHook = ccrz.cc_hk_UserInterface.getInstance(null);

        Map<String, Object> headContentMap = (Map<String, Object>)UserInterfaceHook.headContent(inputData);
        Map<String, Object> endContentMap = (Map<String, Object>)UserInterfaceHook.endContent(inputData);

        System.assertNotEquals(headContentMap, null);
        System.assertNotEquals(endContentMap, null);

        Test.stopTest();
    }

    @isTest
    static void testMethodClassicMin() {
        String STOREFRONT = 'storefront1';

        Map<String,Map<String,Object>> testData = new Map<String,Map<String,Object>>{
            ccrz.ccApiTestData.HOOK_SETTINGS => new Map<String, Object>{
                STOREFRONT => new Map<String,Object>{
                    ccrz.cc_hk_UserInterface.HK_ID => 'c.cc_shop360_hk_UserInterface_v4'
                }
            },
            ccrz.ccApiTestData.CONFIG_SETTINGS => new Map<String,Object>{
                'ui.tmplver' => 'classic',
                'ui.usemin' => 'TRUE',
                'th.name' => 'CC_Theme_Shop360'
            }
        };
        ccrz.ccApiTestData.setupData(testData);

        // Setup of the CCRZ context
        ccrz.cc_RemoteActionContext ctx = new ccrz.cc_RemoteActionContext();

        ctx.storefront = STOREFRONT;
        ctx.userLocale = 'en_US';

        ccrz.cc_CallContext.initRemoteContext(ctx);

        Map<String, Object> inputData = new Map<String, Object>{
            ccrz.ccAPI.API_VERSION => ccrz.ccAPI.CURRENT_VERSION,
            ccrz.ccAPI.SIZING => new Map<String, Object>{
                ccrz.ccApiCategory.ENTITYNAME => new Map<String, Object>{
                    ccrz.ccAPI.SZ_DATA => ccrz.ccAPI.SZ_L
                }
            }
        };

        Test.startTest();
        ccrz.cc_hk_UserInterface UserInterfaceHook = ccrz.cc_hk_UserInterface.getInstance(null);

        Map<String, Object> headContentMap = (Map<String, Object>)UserInterfaceHook.headContent(inputData);
        Map<String, Object> endContentMap = (Map<String, Object>)UserInterfaceHook.endContent(inputData);

        System.assertNotEquals(headContentMap, null);
        System.assertNotEquals(endContentMap, null);

        Test.stopTest();
    }
}