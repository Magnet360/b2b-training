/*****************************************************
 * Author: Zach Engman
 * Created Date: 07/29/2019
 * Created By: Zach Engman
 * Last Modified Date: 07/29/2019
 * Last Modified By:
 * Description: Provides test coverage of the functionality within the cc_shop360_MiniCartWidget_Ctrl class
 * ****************************************************/
@IsTest
private class cc_shop360_MiniCartWidget_Ctrl_Test {
   @TestSetup
    private static void setupTestData(){
        ccrz__E_AccountGroup__c accountGroupRecord = cc_Test_Factory.createCCAccountGroup();
        insert accountGroupRecord;

        Account accountRecord = cc_Test_Factory.createAccount();
        accountRecord.ccrz__E_AccountGroup__c = accountRecord.Id;
        insert accountRecord;

        Contact contactRecord = cc_Test_Factory.createContact();
        contactRecord.AccountId = accountRecord.Id;
        insert contactRecord;

        User b2bUser = cc_Test_Factory.createUser(cc_Test_Factory.B2B_COMMUNITY_PROFILE_NAME);
        b2bUser.ContactId = contactRecord.Id;
        b2bUser.Contact = contactRecord;
        insert b2bUser;

        cc_Test_Factory.setupB2BCatalog(); 
    }

    @IsTest
    private static void testGetCart(){
    	User userRecord = [SELECT Id FROM User WHERE Alias = 'b2btest'];
        String cartResultJSON;
    	cc_shop360_Cart_Service.CartRecord cartResult;
        
        //create the cart record
        ccrz__E_Cart__c cartRecord = cc_Test_Factory.createCCCart();
        cartRecord.ccrz__User__c = userRecord.Id;
        cartRecord.OwnerId = userRecord.Id;
        insert cartRecord;

        //create some cart items
        ccrz__E_CartItem__c cartItemRecord = cc_Test_Factory.createCCCartItem(cartRecord.Id, [SELECT Id FROM ccrz__E_Product__c WHERE ccrz__SKU__c = 'sku001' LIMIT 1].Id);
        insert cartItemRecord;

    	System.runAs(userRecord){
            Test.startTest();

            cartResultJSON = cc_shop360_MiniCartWidget_Ctrl.getCart(cc_Test_Factory.STOREFRONT_NAME, cartRecord.ccrz__EncryptedId__c, 0);

            Test.stopTest();
        }

        //cartResult = (cc_shop360_Cart_Service.CartRecord)JSON.deserializeUntyped(cartResultJSON);
        //verify cart is returned
        System.assert(!String.isBlank(cartResultJSON));
        //System.assertEquals(cartRecord.Id, cartResult.Id);
    }
}