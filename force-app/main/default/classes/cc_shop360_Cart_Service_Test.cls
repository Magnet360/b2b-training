/*****************************************************
 * Author: Zach Engman
 * Created Date: 07/29/2019
 * Created By: Zach Engman
 * Last Modified Date: 07/29/2019
 * Last Modified By:
 * Description: Provides test coverage of the functionality within the cc_shop360_Cart_Service class
 * ****************************************************/
@IsTest
private class cc_shop360_Cart_Service_Test {
    @TestSetup
    private static void setupTestData(){
        ccrz__E_AccountGroup__c accountGroupRecord = cc_Test_Factory.createCCAccountGroup();
        insert accountGroupRecord;

        Account accountRecord = cc_Test_Factory.createAccount();
        accountRecord.ccrz__E_AccountGroup__c = accountRecord.Id;
        insert accountRecord;

        Contact contactRecord = cc_Test_Factory.createContact();
        contactRecord.AccountId = accountRecord.Id;
        insert contactRecord;

        User b2bUser = cc_Test_Factory.createUser(cc_Test_Factory.B2B_COMMUNITY_PROFILE_NAME);
        b2bUser.ContactId = contactRecord.Id;
        b2bUser.Contact = contactRecord;
        insert b2bUser;

        cc_Test_Factory.setupB2BCatalog(); 
    }

    @IsTest
    private static void testGetCartByEncryptedId(){
    	User userRecord = [SELECT Id FROM User WHERE Alias = 'b2btest'];
    	cc_shop360_Cart_Service cartService;
    	cc_shop360_Cart_Service.CartRecord cartResult;

        //create the cart record
        ccrz__E_Cart__c cartRecord = cc_Test_Factory.createCCCart();
        cartRecord.ccrz__User__c = userRecord.Id;
        cartRecord.OwnerId = userRecord.Id;
        insert cartRecord;

        //create some cart items
        ccrz__E_CartItem__c cartItemRecord = cc_Test_Factory.createCCCartItem(cartRecord.Id, [SELECT Id FROM ccrz__E_Product__c WHERE ccrz__SKU__c = 'sku001' LIMIT 1].Id);
        insert cartItemRecord;

    	System.runAs(userRecord){
            Test.startTest();

            cartService = new cc_shop360_Cart_Service(cc_Test_Factory.STOREFRONT_NAME, userRecord.Id, cartRecord.ccrz__EncryptedId__c);
            cartResult = cartService.getCartByEncryptedId(cartRecord.ccrz__EncryptedId__c);

            Test.stopTest();
        }

        //verify cart is returned
        System.assert(cartResult != null);
        System.assertEquals(cartRecord.Id, cartResult.Id);
    }

    @IsTest
    private static void testGetCartByEncryptedIdWithInvalidEncryptedId(){
    	User userRecord = [SELECT Id FROM User WHERE Alias = 'b2btest'];
    	cc_shop360_Cart_Service cartService;
    	cc_shop360_Cart_Service.CartRecord cartResult;

    	System.runAs(userRecord){
            Test.startTest();

            cartService = new cc_shop360_Cart_Service(userRecord.Id,'');
            cartResult = cartService.getCartByEncryptedId('');

            Test.stopTest();
        }

        //verify no cart is returned
        System.assert(cartResult == null);
    }
}