/*****************************************************
 * Author: Zach Engman
 * Created Date: 07/31/2019
 * Created By: Zach Engman
 * Last Modified Date: 07/31/2019
 * Last Modified By:
 * Description: Extension/Override of the cc_hk_Menu (top menu in storefront)
 * ****************************************************/
global inherited sharing class cc_shop360_hk_Menu extends ccrz.cc_hk_Menu{

    @TestVisible
    global boolean useCategoryTree{
        get{if(useCategoryTree == null) {useCategoryTree = ccrz.cc_CallContext.isConfigTrue('tm.usecategorytree', null); } return useCategoryTree;}
        private set;
    }

    global override Map<String,Object> fetch(Map<String,Object> inputData){
        List<ccrz.cc_bean_MenuItem> menuItemList = new List<ccrz.cc_bean_MenuItem>();
 
        //check for menu setting in the top menu
        if(this.useCategoryTree){
            try{
                //retrieve the category tree menu
                Map<String, Object> categoryTreeMap = ccrz.ccAPICategory.getTree(new Map<String, Object>{ccrz.ccApi.API_VERSION => 9});

                if(categoryTreeMap.get(ccrz.ccApiCategory.CATEGORYTREE) != null){
                    List<Object> categoryTreeObjectList = (List<Object>)categoryTreeMap.get(ccrz.ccApiCategory.CATEGORYTREE);
                    for(Object categoryObject : categoryTreeObjectList){
                        Map<String, Object> categoryRecordMap = (Map<String, Object>)categoryObject;
                        ccrz.cc_bean_MenuItem menuItem = new ccrz.cc_bean_MenuItem((String)categoryRecordMap.get('sfid'), (String)categoryRecordMap.get('name'), 'ccrz__ProductList?categoryId=' + (String)categoryRecordMap.get('sfid'), false, 'URL', menuItemList.size());

                        if(categoryRecordMap.get('children') != null){
                            menuItem = addChildren(menuItem, (List<Object>)categoryRecordMap.get('children'));
                        }

                        menuItemList.add(menuItem);
                    }
                }
            }
            catch(Exception ex){
                //load the default
                System.Debug(ex);
                return super.fetch(inputData);
            }

            return new Map<String, Object> {ccrz.cc_hk_Menu.PARAM_MENU => menuItemList};
        }
        else{
             Map<String,Object> baseData = super.fetch(inputData);
             return baseData;
        }
    }

    /**
	* Recursively creates the child menu item components from the parent
	* @param parentMenuItem - the parent cc_bean_MenuItem record
    * @param childList - the list of children returned from the category map with the children key
	* @return the parent menu item with related children added to the children list property
	*/
    private ccrz.cc_bean_MenuItem addChildren(ccrz.cc_bean_MenuItem parentMenuItem, List<Object> childList){
        //first child is hidden so add a placeholder
        parentMenuItem.children.add(new ccrz.cc_bean_MenuItem(parentMenuItem.sfid, parentMenuItem.displayName, parentMenuItem.linkURL, false, 'URL', 0));
        //add child that is all
        parentMenuItem.children.add(new ccrz.cc_bean_MenuItem(parentMenuItem.sfid, 'All ' + parentMenuItem.displayName, parentMenuItem.linkURL, false, 'URL', 0));

        for(Object categoryObject : childList){
            Map<String, Object> categoryRecordMap = (Map<String, Object>)categoryObject;
            ccrz.cc_bean_MenuItem childMenuItem = new ccrz.cc_bean_MenuItem((String)categoryRecordMap.get('sfid'), (String)categoryRecordMap.get('name'), 'ccrz__ProductList?categoryId=' + (String)categoryRecordMap.get('sfid'), false, 'URL',parentMenuItem.children.size());
            //recursively call in case of more children
            if(categoryRecordMap.get('children') != null){
                childMenuItem = addChildren(childMenuItem, (List<Object>)categoryRecordMap.get('children'));
            }

            parentMenuItem.children.add(childMenuItem);
        }

        return parentMenuItem;
    }
}