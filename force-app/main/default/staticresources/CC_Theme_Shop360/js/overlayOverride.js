//CustomEvent polyfill for IE9 and later; can be removed if IE support is not needed
(function() {
  if (typeof window.CustomEvent === "function") return false;

  function CustomEvent(event, params) {
    params = params || { bubbles: false, cancelable: false, detail: null };
    var evt = document.createEvent("CustomEvent");
    evt.initCustomEvent(
      event,
      params.bubbles,
      params.cancelable,
      params.detail
    );
    return evt;
  }

  window.CustomEvent = CustomEvent;
})();

//set custom overlayHidden event
var overlayEvent = new CustomEvent("overlayHidden", {
  detail: {
    overlayHidden: true
  }
});

//document on ready function
function ready(fn) {
  if (
    document.attachEvent
      ? document.readyState === "complete"
      : document.readyState !== "loading"
  ) {
    fn();
  } else {
    document.addEventListener("DOMContentLoaded", fn);
  }
}

function addOverlay() {
  //adds top padding to the body when the progress bar indicator is shown
  document.body.classList.add("overlay-padding");
}

function removeOverlay() {
  //removes top padding to the body when the progress bar indicator is shown
  document.body.classList.remove("overlay-padding");
  //removes class the hides overlay for button actions
  document.body.classList.remove("overlay-hide");
}

function hideStencils() {
  //hides stencil UI for components that are not completely empty
  if (document.querySelector(".minicart")) {
    if (
      document.querySelector(".minicart").innerHTML.length == 0 &&
      CCRZ.miniCartView == undefined
    ) {
      document.querySelector(".minicart").style.display = "none";
    } else {
      document.querySelector(".minicart").style.display = "";
    }
  }

  if (document.querySelector(".cart-template")) {
    if (
      document.querySelector(".cart-template").innerHTML.length == 0 &&
      CCRZ.quickWishEntryView == undefined
    ) {
      document.querySelector(".cart-template").style.display = "none";
    } else {
      document.querySelector(".cart-template").style.display = "";
    }
  }
}

function addButtonLoader(event) {
  //hide page level progress indicator and display spinner for button
  document.body.classList.add("overlay-hide");
  event.currentTarget.classList.add("btn-loader");
}

function removeButtonLoader() {
  //hide button spinner
  var btnLoaders = document.querySelectorAll(".btn-loader"),
    btnLoaderRes;
  for (var i = 0; i < btnLoaders.length; i++) {
    btnLoaders[i].classList.remove("btn-loader");
  }
}

function overlayOverride() {
  //monitors the DOM for the "overlay" being added to the page to override the UI/UX
  // set up the mutation observer
  var overlayParent = document.querySelector(".deskLayout");
  var hasOverlay = false;
  var overlayCount = 0;
  var overlayObserver = new MutationObserver(function(mutations, me) {
    // 'mutations' is an array of mutations that occurred
    // 'me' is the MutationObserver instance
    for (var index = 0; index < mutations.length; index++) {
      var mutation = mutations[index];

      //maintain count of 'overlay' elements on the page
      if (mutation.type === "childList" && mutation.addedNodes.length) {
        if (mutation.addedNodes[0].id === "overlay") {
          overlayCount = overlayCount + 1;
        }
      }

      if (mutation.type === "childList" && mutation.removedNodes.length) {
        if (mutation.removedNodes[0].id === "overlay") {
          overlayCount = overlayCount - 1;
        }
      }

      //determine if overlay elements exist on the page
      if (overlayCount > 0) {
        hasOverlay = true;
        //listen for overlayHidden event to hide stencils for widgets that are not enabled
        window.addEventListener("overlayHidden", function(e) {
          hideStencils();
        });
      } else {
        hasOverlay = false;
        //fire event to handle overlay is removed
        window.dispatchEvent(overlayEvent);
      }

      // are the OOTB overlay elements present?
      if (hasOverlay) {
        addOverlay();
      } else {
        removeButtonLoader();
        removeOverlay();
      }
    }
  });

  // start observing
  overlayObserver.observe(overlayParent, {
    childList: true,
    subtree: true
  });

  //Add to Cart button spinner override for mini cart view
  CCRZ.pubSub.on("view:cartView:refresh", function(viewRef) {
    var cartUpdateBtns = document.querySelectorAll(".updateCartButton"),
      cartUpdateResult;
    for (var i = 0; i < cartUpdateBtns.length; i++) {
      cartUpdateResult = cartUpdateBtns[i];
      cartUpdateResult.addEventListener("click", function(event) {
        addButtonLoader(event);
      });
    }
  });

  //Add to Cart button spinner override for product detail page
  CCRZ.pubSub.on("view:productDetailView:refresh", function(viewRef) {
    var addToCartBtns = document.querySelectorAll(".addItem"),
      addToCartResult;
    for (var i = 0; i < addToCartBtns.length; i++) {
      addToCartResult = addToCartBtns[i];
      addToCartResult.addEventListener("click", function(event) {
        addButtonLoader(event);
      });
    }
  });

  //Add to Cart button spinner override for product list page
  CCRZ.pubSub.on("view:productItemView:refresh", function(viewRef) {
    viewRef.$el.find(".cc_add_to_btn").on("click", function(event) {
      addButtonLoader(event);
    });
  });

  //Add to Wishlist button spinner override for wishlist component
  CCRZ.pubSub.on("view:wishlistPickerModal:refresh", function(viewRef) {
    viewRef.$el.find(".pickWish").on("click", function(event) {
      addButtonLoader(event);
    });
  });

  //hide product list page content container if no results to hide stencil UI
  CCRZ.pubSub.on("view:productItemsView:refresh", function(viewRef) {
    if (viewRef.itemViews.length === 0) {
      document.querySelector(".productListContent").style.display = "none";
    } else {
      document.querySelector(".productListContent").style.display = "";
    }
  });

  //Update button spinner override for cart page
  CCRZ.pubSub.on("view:CartDetailView:refresh", function(viewRef) {
    viewRef.$el.find(".updateCartButton").on("click", function(event) {
      if (viewRef.params.hasChanged) {
        addButtonLoader(event);
      }
    });

    viewRef.$el.find(".removeItemButton").on("click", function(event) {
      addButtonLoader(event);
    });
  });

  //hide wishlist component on home page if no results to hide stencil UI
  CCRZ.pubSub.on("view:quickWishlistView:refresh", function(viewRef) {
    if (viewRef.wishlistData.length === 0) {
      document.querySelector(".cart-template").style.display = "none";
    } else {
      document.querySelector(".cart-template").style.display = "";
    }
  });

  //set timeout to hide stencils in case overlay never loads
  //adjust timing as needed
  setTimeout(function() {
    hideStencils();
  }, 2000);
}

ready(overlayOverride);
