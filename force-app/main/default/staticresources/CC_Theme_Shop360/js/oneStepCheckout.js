/***
 * JS used on cc_shop360_CheckoutNew_PI
 * Bryan Anderson - Magnet360 - 2019/06/06
 ***/

//set template for the Checkout header
CCRZ.uiProperties.CheckoutNav.desktop.selector = "";
CCRZ.uiProperties.CheckoutNav.desktop.tmpl = "CheckoutHeader-Empty-Desktop";

//configure Checkout viw for One Step view
CCRZ.Checkout = _.extend(CCRZ.Checkout || {}, {
  OneStepView: {
    register: function(registrar) {
      registrar.registerViewNew(
        "OneStepView",
        new CCRZ.views.OneStepView(),
        ""
      );
    }
  }
});

function checkoutOrderReviewOrientationResize() {
  //handle collapse functionality of Order Review view based on media query
  //not currently accessible due to OOTB template heading not focusable
  if (window.matchMedia("(max-width: 991px)").matches) {
    $(".container-order-review .panel-body").collapse("hide");
    $(".container-order-review .panel-title").addClass("collapsed");
  } else {
    $(".container-order-review .panel-body").collapse("show");
    $(".container-order-review .panel-title").removeClass("collapsed");
  }
}

//check for window resize and bind accordingly
$(window).on("resize", function() {
  checkoutOrderReviewOrientationResize();
});

//check for the orientation event and bind accordingly
if (window.DeviceOrientationEvent) {
  window.addEventListener(
    "orientationchange",
    checkoutOrderReviewOrientationResize,
    false
  );
}

jQuery(function($) {
  //create One Step view
  CCRZ.views.OneStepView = CCRZ.CloudCrazeView.extend({
    templatePhone: CCRZ.util.template("OneStep-TwoColumn-Desktop"),
    templateDesktop: CCRZ.util.template("OneStep-TwoColumn-Desktop"),
    viewName: "OneStepView",
    managedSubView: true,
    events: {
      "click .place-order": "placeOrder",
      "click .go-to-cart": "goToCart",
      "click .container-order-review .panel-title": "collapseOrderReview"
    },
    init: function() {
      // handle Terms checkbox error class
      $(document).on('click', '.terms', function(){
        if ($(this).prop('checked')) {
            $(this).removeClass('error');
        }
      });
    },
    initSetup: function(callback) {
      callback();
    },
    renderPhone: function() {
      this.renderView(this.templatePhone);
    },
    renderDesktop: function() {
      this.renderView(this.templateDesktop);
    },
    renderView: function(currTemplate) {
      //swapped 'this' for 'CCRZ.cartCheckoutModel.toJSON()' to add cart model data to view
      this.$el.html(currTemplate(CCRZ.cartCheckoutModel.toJSON()));
    },
    setUserInfoModel: function(callback) {
      //retrieve form data for User Info and update model to allow other views to render properly
      //some code from 'CCRZ.views.UserInfoView.prototype.processUserInfo' function
      var formData = form2js("mainform", ".", false, function(node) {}, false);
      CCRZ.cartCheckoutModel.set(formData);
      if ($("#ccEmailForm").length > 0) {
        //retrieve form data for email address and update model
        var ccFormData = form2js(
          "ccEmailForm",
          ".",
          false,
          function(node) {},
          false
        );
        CCRZ.cartCheckoutModel.set(ccFormData);
      }
      //retrieve form data for billing address and update model
      var billData = form2js(
        "billingAddressForm",
        ".",
        false,
        function(node) {},
        false
      );
      billData.billingAddress.country = $(
        ".billingcountry option:selected"
      ).text();
      CCRZ.cartCheckoutModel.set(billData);
      //retrieve form data for shipping address and update model
      var shipData = form2js(
        "shippingAddressForm",
        ".",
        false,
        function(node) {},
        false
      );
      shipData.shippingAddress.country = $(
        ".shippingcountry option:selected"
      ).text();
      CCRZ.cartCheckoutModel.set(shipData);
      //re-initialize addresses
      CCRZ.OneStepUserInfoView.initAddresses();

      if (callback) {
        callback();
      }
    },
    placeOrder: function(e) {
      e.preventDefault();

      var currentTarget = e.currentTarget;

      var v = this;

      var isUserInfoValid = false;
      var isShippingValid = false;

      //check if Terms are enabled for Shipping view
      var terms = CCRZ.cartCheckoutModel.get("terms").length;
      var isTermsValid = false;
      var checkedTerms = $(".terms:checked").length;

      //get active payment type for checkout
      var activeTab = $(".cc_nav-tabs-payment li.active a").attr("href");

      isUserInfoValid = CCRZ.OneStepUserInfoView.validateInfo();
      isShippingValid = CCRZ.OneStepShippingView.validateInfo();

      //check if terms are valid
      if (terms == 0 || terms == checkedTerms) {
        isTermsValid = true;
      } else {
        isTermsValid = false;
      }

      //are the user info and shipping forms valid?
      if (isUserInfoValid && isShippingValid && isTermsValid) {
        this.setUserInfoModel();

        //reset error messaging
        $(
          ".main-messagingSection-Error, .messagingSection-Error, .messagingSection-Info, .messagingSection-Warning"
        ).hide();
        $("#terms").removeClass("error");

        //disable Place Order button
        currentTarget.disabled = true;

        //execute appropriate function to initialize place order function based on current active payment type
        //update if additional payment types are added to checkout
        switch (activeTab) {
          case "#po":
            v.processPOOrder(currentTarget);
            break;
          case "#cc":
            v.processCCOrder(currentTarget);
            break;
          default:
            break;
        }
      } else if (!isUserInfoValid) {
        //scroll to User Info section if form validation fails
        $("html, body").animate(
          {
            scrollTop: $(".container-user-info").offset().top
          },
          150,
          function() {
            //re-enable Place Order button
            currentTarget.disabled = false;
          }
        );
      } else if (!isShippingValid) {
        //scroll to Shipping section if form validation fails
        $("html, body").animate(
          {
            scrollTop: $(".container-shipping").offset().top
          },
          150,
          function() {
            //re-enable Place Order button
            currentTarget.disabled = false;
          }
        );
      } else if (!isTermsValid) {
        //scroll to Shipping section if form validation fails
        $("html, body").animate(
          {
            scrollTop: $(".cc_coso_footer_tc").offset().top
          },
          150,
          function() {
            // add error class to Terms checkbox
            $(".terms").addClass("error");
            //re-enable Place Order button
            currentTarget.disabled = false;
          }
        );
      }
    },
    goToCart: function(e) {
      //navigate back to Cart page
      e.preventDefault();
      //OOTB B2B JS function
      cartDetails();
    },
    processPOOrder: function(target) {
      //retrieve form data and process order if form data is valid; if form validation fails scroll to Payment view section
      //some code from 'CCRZ.views.PaymentsPOView.prototype.makePOPayment' function
      var formName = "newWalletForm";
      var formData = form2js(formName, ".", false, function(node) {}, false);
      if (
        CCRZ.views.PaymentsPOView.prototype.validateInfo(formName, formData)
      ) {
        $(".poPayment-messagingSection-Error").hide();
        //save form data to update Cart record
        CCRZ.cartCheckoutModel.save(function(response) {
          if (response) {
            formData.accountType = "po";
            CCRZ.models.PaymentModel.prototype.processPayment(formData);
          } else {
            CCRZ.pubSub.trigger("pageMessage", {
              messages: [
                {
                  type: "CUSTOM",
                  labelId: "Checkout_OrderPlaceError",
                  severity: "ERROR",
                  classToAppend: "messagingSection-Error"
                }
              ]
            });
          }
        });
      } else {
        //scroll to Payment section if form validation fails
        $("html, body").animate(
          {
            scrollTop: $(".container-payment").offset().top
          },
          150,
          function() {
            //re-enable Place Order button
            target.disabled = false;
          }
        );
      }
    },
    processCCOrder: function(target) {
      //retrieve form data and process order if form data is valid; if form validation fails scroll to Payment view section
      //some code from 'CCRZ.views.PaymentsPOView.prototype.makePOPayment' function
      var formName = "newWalletCCForm";
      var formData = form2js(formName, ".", false, function(node) {}, false);
      //mask credit card number - remove or adjust as needed for credit card processing
      formData.accountNumber = formData.accountNumber.replace(
        /\d(?=\d{4})/g,
        "*"
      );
      //parse the expiration month from credit card payment form
      formData.expirationMonth = parseInt(
        formData.ccExpirationDate.split("/")[0]
      );
      //parse the expiration year from credit card payment form
      formData.expirationYear = parseInt(
        "20" + formData.ccExpirationDate.split("/")[1]
      );

      //if payment form data is valid save data to CC Cart record and process payment to create CC Order; otherwise, scroll to error on page
      if (
        CCRZ.views.PaymentsCCView.prototype.validateInfo(formName, formData)
      ) {
        $(".ccPayment-messagingSection-Error").hide();
        //save form data to update CC Cart record
        CCRZ.cartCheckoutModel.save(function(response) {
          if (response) {
            formData.accountType = "cc";
            CCRZ.models.PaymentModel.prototype.processPayment(formData);
          } else {
            CCRZ.pubSub.trigger("pageMessage", {
              messages: [
                {
                  type: "CUSTOM",
                  labelId: "Checkout_OrderPlaceError",
                  severity: "ERROR",
                  classToAppend: "messagingSection-Error"
                }
              ]
            });
          }
        });
      } else {
        //scroll to Payment section if form validation fails
        $("html, body").animate(
          {
            scrollTop: $(".container-payment").offset().top
          },
          150,
          function() {
            //re-enable Place Order button
            target.disabled = false;
          }
        );
      }
    },
    collapseOrderReview: function(e) {
      //handle collapse functionality of Order Review view based on media query
      //not currently accessible due to OOTB template heading not focusable
      e.preventDefault();
      if (window.matchMedia("(max-width: 991px)").matches) {
        if ($(".container-order-review .panel-body").is(":visible")) {
          $(".container-order-review .panel-body").collapse("hide");
          e.currentTarget.classList.add("collapsed");
        } else {
          $(".container-order-review .panel-body").collapse("show");
          e.currentTarget.classList.remove("collapsed");
        }
      }
    }
  });

  CCRZ.pubSub.on("view:cartCheckoutView:awaitingSubViewInit", function(
    theView
  ) {
    //register One Step Checkout view
    if (CCRZ.Checkout.OneStepView) {
      CCRZ.Checkout.OneStepView.register(theView);
    }
    //initialize subView of Checkout view
    CCRZ.pubSub.trigger("view:cartCheckoutView:subViewInit");
  });

  CCRZ.pubSub.on("view:OneStepView:refresh", function(viewRef) {
    //initialize and render User Info view
    CCRZ.OneStepUserInfoView = new CCRZ.views.UserInfoView();
    CCRZ.OneStepUserInfoView.setElement($(".container-user-info"));
    CCRZ.OneStepUserInfoView.initSetup(function() {
      CCRZ.OneStepUserInfoView.render();

      //wait for render of UserInfo and then render other views
      CCRZ.views.OneStepView.prototype.setUserInfoModel(function() {
        //initialize and render Shipping view
        CCRZ.OneStepShippingView = new CCRZ.views.ShippingView();
        CCRZ.OneStepShippingView.setElement($(".container-shipping"));
        CCRZ.OneStepShippingView.init();
        CCRZ.OneStepShippingView.initSetup(function() {
          CCRZ.OneStepShippingView.render();
        });

        //initialize and render Order Review view
        CCRZ.OneStepOrderReviewView = new CCRZ.views.OrderReviewView();
        CCRZ.OneStepOrderReviewView.setElement($(".container-order-review"));
        CCRZ.OneStepOrderReviewView.init();
        CCRZ.OneStepOrderReviewView.render();
        $(".container-order-review .panel-body").collapse();

        //initialize and render Payment view
        CCRZ.OneStepPaymentView = new CCRZ.views.PaymentView({
          model: new CCRZ.models.PaymentModel()
        });
        CCRZ.OneStepPaymentView.setElement($(".container-payment"));
        CCRZ.OneStepPaymentView.initSetup(function() {
          CCRZ.OneStepPaymentView.render();
        });
      });
    });
  });

  CCRZ.pubSub.on("view:PaymentView:refresh", function(viewRef) {
    //bind function for tab toggle of payment types
    //update if adding additional payment types
    $('.cc_payment_types_container a[data-toggle="tab"]').on(
      "shown.bs.tab",
      function() {
        //reset Credit Card payment form validation
        if ($("#newWalletCCForm").data("validator")) {
          $("#newWalletCCForm")
            .data("validator")
            .resetForm();
        }
        //reset Purchase Order payment form validation
        if ($("#newWalletForm").data("validator")) {
          $("#newWalletForm")
            .data("validator")
            .resetForm();
          $(".poPayment-messagingSection-Error").hide();
        }
      }
    );
  });
});
