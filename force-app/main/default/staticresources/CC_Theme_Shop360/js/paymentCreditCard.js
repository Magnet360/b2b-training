jQuery(function($) {
  //define credit card payment templates
  CCRZ.uiProperties.PaymentsCCView = {
    desktop: {
      tmpl: "PaymentCC-Both"
    },
    phone: {
      tmpl: "PaymentCC-Both"
    }
  };

  //setup Credit Card payment model and view
  CCRZ.models.PaymentsCCModel = CCRZ.CloudCrazeModel.extend();
  CCRZ.views.PaymentsCCView = CCRZ.CloudCrazeView.extend({
    viewName: "PaymentsCCView",
    managedSubView: true,
    templateDesktop: CCRZ.util.template(
      CCRZ.uiProperties.PaymentsCCView.desktop.tmpl
    ),
    templatePhone: CCRZ.util.template(
      CCRZ.uiProperties.PaymentsCCView.phone.tmpl
    ),
    events: {
      "keyup #accountNumber": "detectCardType",
      "input #accountNumber": "detectCardType",
      "keypress .numeric": "isNumeric"
    },
    init: function(options) {
      this.selector = options.selector;
      this.render();
      //initialize payment view event
      CCRZ.pubSub.trigger("action:paymentViewInit", this);

      //validation method for checking for valid credit card number
      //supports Visa, MasterCard, American Express, and Discover; regex can be adjusted to support more
      $.validator.addMethod(
        "validCreditCard",
        function(value, element) {
          var cardType = undefined;
          var cardnumber = value.replace(/[ -]/g, "");
          // See if the card is valid
          // The regex will capture the number in one of the capturing groups
          // supports Visa, Mastercard, Discover, and American Express - update if additional card types are needed
          var match = /^(?:(4[0-9]{12}(?:[0-9]{3})?)|(5[1-5][0-9]{14})|(6(?:011|5[0-9]{2})[0-9]{12})|(3[47][0-9]{13})|(3(?:0[0-5]|[68][0-9])[0-9]{11})|((?:2131|1800|35[0-9]{3})[0-9]{11}))$/.exec(
            cardnumber
          );
          if (match) {
            // List of card types, in the same order as the regex capturing groups
            var types = [
              CCRZ.processPageLabelMap("PaymentType_Visa").string,
              CCRZ.processPageLabelMap("PaymentType_MasterCard").string,
              CCRZ.processPageLabelMap("PaymentType_Discover").string,
              CCRZ.processPageLabelMap("PaymentType_AmericanExpress").string
            ];
            // Skip the zeroth element of the match array (the overall match)
            for (var i = 1; i < match.length; i++) {
              if (match[i]) {
                cardType = types[i - 1];
                break;
              }
            }
          }
          return cardType != undefined ? true : false;
        },
        CCRZ.pagevars.pageLabels["PaymentType_InvalidCreditCard"]
      );

      //validation method for checking for valid MM/YY expiration date
      $.validator.addMethod(
        "validExpiry",
        function(value, element) {
          var today = new Date();
          var thisYear = today.getFullYear();

          var expMonth = parseInt(value.split("/")[0]);
          var expYear = parseInt("20" + value.split("/")[1]);

          // expiration date is valid for current month up to 20 years into the future
          var isValid =
            expMonth >= 1 &&
            expMonth <= 12 &&
            (expYear >= thisYear && expYear < thisYear + 20) &&
            (expYear == thisYear ? expMonth >= today.getMonth() + 1 : true);

          return isValid;
        },
        "Must be a valid Expiry Date"
      );

      //initialize the form validation rules, messages, and configuration
      $("#newWalletCCForm").validate({
        rules: {
          accountName: { required: true },
          paymentType: {
            required: true,
            minlength: 1
          },
          accountNumber: {
            required: true,
            digits: true,
            validCreditCard: true
          },
          ccExpirationDate: {
            required: true,
            minlength: 5,
            maxlength: 5,
            validExpiry: true
          },
          ccCode: {
            required: true,
            digits: true,
            rangelength: function() {
              var cardType = $("#paymentType").val();
              //determine card type and set validation rules for CVV Code form field
              if (cardType) {
                if (
                  cardType ==
                  CCRZ.processPageLabelMap("PaymentType_AmericanExpress")
                ) {
                  //Amex
                  return [4, 4];
                } else {
                  return [3, 3];
                }
              }
            }
          }
        },
        onkeyup: false,
        focusCleanup: true,
        messages: {
          accountName: {
            required: CCRZ.pagevars.pageLabels["PMTCC_CCCardholderNameReq"]
          },
          paymentType: {
            required: CCRZ.pagevars.pageLabels["PMTCC_CCTypeReq"],
            minlength: CCRZ.pagevars.pageLabels["PMTCC_CCTypeReq"]
          },
          accountNumber: {
            required: CCRZ.pagevars.pageLabels["PMTCC_CCNumberReq"],
            digits: CCRZ.pagevars.pageLabels["PMTCC_CCNumberInvalidNumeric"],
            validCreditCard: CCRZ.pagevars.pageLabels["PMTCC_CCNumberInvalid"]
          },
          ccExpirationDate: {
            required: CCRZ.pagevars.pageLabels["PMTCC_CCDateReq"],
            minlength: CCRZ.pagevars.pageLabels["PMTCC_CCDateInvalid"],
            maxlength: CCRZ.pagevars.pageLabels["PMTCC_CCDateInvalid"],
            validExpiry: CCRZ.pagevars.pageLabels["PMTCC_CCDateInvalid"]
          },
          ccCode: {
            required: CCRZ.pagevars.pageLabels["PMTCC_CCCodeReq"],
            digits: CCRZ.pagevars.pageLabels["CheckOut_CCCardCVVInvalidDigits"],
            rangelength: function(value) {
              if (value[0] === value[1]) {
                if (value[0] === 3) {
                  return CCRZ.pagevars.pageLabels["CheckOut_CCCardCVVInvalid3"];
                } else if (value[0] === 4) {
                  return CCRZ.pagevars.pageLabels["CheckOut_CCCardCVVInvalid4"];
                }
              } else {
                return CCRZ.pagevars.pageLabels["CheckOut_CCCardCVVInvalid"];
              }
            }
          }
        },
        showErrors: function(errorMessage, errorMap, errorList) {
          this.defaultShowErrors();
        },
        errorPlacement: function(error, element) {
          error.insertAfter(element);
        }
      });
    },
    validateInfo: function(formName) {
      return $("#" + formName).valid();
    },
    renderDesktop: function() {
      this.setElement(this.selector);
      this.data = {};
      this.$el.html(this.templateDesktop(this.data));
    },
    renderPhone: function() {
      this.setElement(this.selector);
      this.data = {};
      this.$el.html(this.templatePhone(this.data));
    },
    isNumeric: function(e) {
      //allows only numbers to be entered into input
      var key = e.key;

      if (isNaN(key)) {
        e.preventDefault();
      }
    },
    isDate: function(e) {
      //allows only numbers or '/' to be entered into input
      var key = e.key;

      if (isNaN(key) || key != "/") {
        e.preventDefault();
      }
    },
    detectCardType: function(event) {
      // Strip spaces and dashes
      var targetValue = event.currentTarget.value;
      var cardnumber = targetValue.replace(/[ -]/g, "");
      // See if the card is valid
      // The regex will capture the number in one of the capturing groups
      // supports Visa, Mastercard, Discover, and American Express - update if additional card types are needed
      var match = /^(?:(4[0-9]{12}(?:[0-9]{3})?)|(5[1-5][0-9]{14})|(6(?:011|5[0-9]{2})[0-9]{12})|(3[47][0-9]{13})|(3(?:0[0-5]|[68][0-9])[0-9]{11})|((?:2131|1800|35[0-9]{3})[0-9]{11}))$/.exec(
        cardnumber
      );
      if (match) {
        // List of card types, in the same order as the regex capturing groups
        var types = [
          CCRZ.processPageLabelMap("PaymentType_Visa").string,
          CCRZ.processPageLabelMap("PaymentType_MasterCard").string,
          CCRZ.processPageLabelMap("PaymentType_Discover").string,
          CCRZ.processPageLabelMap("PaymentType_AmericanExpress").string
        ];
        // Find the capturing group that matched
        // Skip the zeroth element of the match array (the overall match)
        for (var i = 1; i < match.length; i++) {
          if (match[i]) {
            // Display the card type for that group
            $("#paymentType").val(types[i - 1]);
            //set data-type to display appropriate credit card icon
            $(".card-number-wrapper").attr("data-type", types[i - 1]);
            break;
          }
        }
      } else {
        //reset credit card type and icon if no match is found for valid number
        $("#paymentType").val("");
        $(".card-number-wrapper").removeAttr("data-type");
      }

      //adjust maxlength for credit card CVV code based on card type
      //truncates number if changed from American Express to other card types
      var cardType = $("#paymentType").val();
      if (cardType) {
        if (
          cardType == CCRZ.processPageLabelMap("PaymentType_AmericanExpress")
        ) {
          //Amex
          $("#ccCode").attr("maxlength", 4);
        } else {
          $("#ccCode").attr("maxlength", 3);
          $("#ccCode").val(
            $("#ccCode")
              .val()
              .substring(0, 3)
          );
        }
      }
    }
  });
  CCRZ.pubSub.trigger("action:paymentViewReady", "cc", function(options) {
    CCRZ.payment = CCRZ.payment || { views: {} };
    CCRZ.payment.views.cc = new CCRZ.views.PaymentsCCView({
      model: new CCRZ.models.PaymentsCCModel(),
      selector: options.selector
    });
  });
});