import { LightningElement, api, wire, track } from 'lwc';
import getCart from '@salesforce/apex/cc_shop360_MiniCartWidget_Ctrl.getCart';

export default class MiniCartWidget extends LightningElement {
    @api cart = {};
    
    @api userId;
    @api encryptedCartId;
    @api storefront = 'Default'; //set to default in case it is not passed
    @api refreshCount = 0;
    @api labelCartNavigation = 'Go To Cart'; //set default in case no label is passed
    @api labelNoCartItems = 'You have no items in your shopping cart.'; //set default in case no label is passed
    @api labelQuantity = 'Quantity'; //set default in case no label is passed
    @api labelSubtotal = 'Cart Subtotal'; //set default in case no label is passed
    @api popover;
    @api price;
    @track error;
    getCartResult;

    @wire(getCart, {storefront : '$storefront', encryptedCartId : '$encryptedCartId', refreshCount : '$refreshCount'})
    wiredCart({error, data}){
        if(error){
            this.error = error;
        } 
        else if (data){
            this.getCartResult = JSON.parse(data);
            if(this.getCartResult.success && this.getCartResult.cart){
                this.getCartResult.cart.subTotalDisplay = this.price(this.getCartResult.cart.subtotalAmount);
                if(this.getCartResult.cart.cartItems){
                    this.getCartResult.cart.cartItems.forEach(function(item){
                        item.itemTotalDisplay = this.price(item.itemTotal);
                    }, this);
                }
                this.cart = this.getCartResult.cart;
            } 
            else if(this.popover){  
                this.popover(true);
            }
        }
    }

    renderedCallback() {
        if(this.popover && this.cart.id){
            this.popover(true);
        } 
    }
}